/*i
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 * 		www.cplusplus.com on - I looked up the string and set functions
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 6
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;

unsigned long countWords(const string& s, set<string>& wl){
/* 	the index variable keeps track of what the start of the new word is */
	int index = 0;
	unsigned long num_words = 0;
	string ns; 							//This sentence will not have any beginning or trailing spaces/tabs
	int ns_start = 0;
	int ns_end = s.length();

/* 	Checks for beginning and trailing white spaces/tabs and trims them */
	if (s[ns_start]==' ' || s[ns_start]=='\t'){
		ns_start=s.find_first_not_of(" \t");
	}
	if (s[ns_end-1]==' ' || s[ns_end-1]=='\t'){
		ns_end=s.find_last_not_of(" \t");
	}
	ns=s.substr(ns_start, ns_end-ns_start+1);
		
	for (unsigned i = 0; i < ns.length(); i++){
/* 		This conditional checks for multiple spaces/tabs between words */
		if (ns[index]==' ' || ns[index]=='\t'){
			index++;
			continue;
		}
/* 		This conditional checks if the end of the string has been reached */		
		if (i==ns.length()-1){
			wl.insert(ns.substr(index,i-index+1));
			num_words++;
			break;
		}
		if (ns[i]==' ' || ns[i]=='\t'){
			wl.insert(ns.substr(index,i-index));
			index=i+1;
			num_words++;
		}
	}
	return num_words;
}

int main()
{
	unsigned long num_words = 0;
	unsigned long num_lines = 0;
	unsigned long num_chars = 0;
	string line;

	set<string> uni_line;
	set<string> uni_word;

	while (getline(cin,line)){
		num_words+=countWords(line,uni_word);
		num_chars+=line.length()+1;
		num_lines++;
		uni_line.insert(line);
	}

	cout << num_lines << "\t" << num_words << "\t" << num_chars << "\t" << uni_line.size() << "\t" << uni_word.size() << endl;
	return 0;
}
