/*
 * CSc103 Project 4: Triangles
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *	http://stackoverflow.com/questions/19340607/congruence-similarity-and-right-triangles - 
 *		I used this site to figure out why my similar function wasn't working properly. My original similar function compared the ratios of the sides of two triangles. This caused problems because integer division doesn't 
 *      guarantee that the result will be an integer. Moreover, in C++, integer division throws away everything to the right of the decimal. This means, for example, that two triangles t31(24,45,51) and t32(25,60,65) will have
 *		a ratio of less than 1. However, C++ integer division would remove the remainder and output a ratio of 0, which would indicate that the two triangles are similar, but they aren't. So instead of dividing by the ratios, 
 *		I cross-multiply the ratios. This works because integer multiplication guarantees that the result will be an integer.
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 6 hours
 */

#include "triangles.h" // import the prototypes for our triangle class.
#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;

// note the "triangle::" part.  We need to specify the function's
// FULL name to avoid confusion.  Else, the compiler will think we
// are just defining a new function called "perimeter"
unsigned long triangle::perimeter() {
	return s1+s2+s3;
}

unsigned long triangle::area() {
	// Note: why is it okay to return an integer here?  Recall that
	// all of our triangles have integer sides, and are right triangles...
/*	It is ok to return an integer because the two legs are either of opposite parity (one even, one odd) or both even, but never both odd. This means that a*b will always result in an even number,
 *	which is divisible by 2.
 */
	unsigned long sides[3]={s1,s2,s3};						// Put the sides in an array
	sort(sides, sides+3);
	return sides[0]*sides[1]/2;
}

void triangle::print() {
	cout << "[" << s1 << "," << s2 << "," << s3 << "]\n";
}

bool congruent(const triangle& t1, const triangle& t2) {
	unsigned long t1_sides[3]={t1.s1,t1.s2,t1.s3};
	unsigned long t2_sides[3]={t2.s1,t2.s2,t2.s3};
	sort(t1_sides, t1_sides+3);
	sort(t2_sides, t2_sides+3);

// I check for congruence by sorting the sides of both triangles in ascending order and checking that the sides of the two triangles are equal
	if (t1_sides[0]==t2_sides[0] && t1_sides[1]==t2_sides[1] && t1_sides[2]==t2_sides[2])
		return true;
	return false;
}

bool similar(const triangle& t1, const triangle& t2) {
	unsigned long t1_sides[3]={t1.s1,t1.s2,t1.s3};
	unsigned long t2_sides[3]={t2.s1,t2.s2,t2.s3};
	sort(t1_sides, t1_sides+3);
	sort(t2_sides, t2_sides+3);

// I check for similarity by cross-multiplying the ratios of the two triangles because integer division can cause problems. See the reference above for more details.	
	if ( (t2_sides[0]*t1_sides[1] == t2_sides[1]*t1_sides[0]) && (t2_sides[1]*t1_sides[2] == t2_sides[2]*t1_sides[1]) && (t2_sides[2]*t1_sides[0] == t2_sides[0]*t1_sides[2]) )
		return true;
	return false;
}

vector<triangle> findRightTriangles(unsigned long l, unsigned long h) {
	// TODO: find all the right triangles with integer sides,
	// subject to the perimeter bigger than l and less than h
	vector<triangle> retval; // storage for return value.
	unsigned long perim=0;
	
	for (unsigned long s1=1; s1<=h; s1++){
		for (unsigned long s2=s1; s2<=h-s1; s2++){
			for (unsigned long s3=s2; s3<=h-s1-s2; s3++){
				perim=s1+s2+s3;
				if (perim>=l && perim<=h){
					unsigned long sides[3]={s1,s2,s3};
					sort(sides,sides+3);
					if (sides[0]*sides[0]+sides[1]*sides[1]==sides[2]*sides[2]){
//						cout << "sides: " << sides[0] << ", " << sides[1] << ", " << sides[2] << "\n";
						triangle rt(sides[0],sides[1],sides[2]);
						retval.push_back(rt);
					}
				}
			}
		}
	}
	
	return retval;
}

