/*
 * CSc103 Project 2: prime numbers.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <cmath>

int main()
{
	signed long n;  									// Contains the integer input from user.
	int max; 											// The max number to divide an integer.	

	while ( cin >> n ){
		if ( (n % 2 == 0 && n != 2) || n <= 1 ) 		// Checks if either input is even and not 2 or n is less than or equal to 1. If true, n is not prime.
			cout << "0" << endl;
		else if ( n == 2 || n == 3 || n == 5 ){ 		// Further checks if input is 2,3 or 5 and outputs 1 if this condition is true.
				cout << "1" << endl;
		} else{
			max = (int)ceil((double)n/2); 				// The max number is determined by rounding up (n/2) and then typecasting into an int to prevent those warning signs when the file is compiled.
			for (int i = 3; i < max; i+=2){ 			// This for loop divides the input by an odd integer, starting with 3 and ending with the max number.
				if ( n % i == 0 ) { 					// If the input is divisible by any odd integer, then the input is not a prime number and outputs 0.
					cout << "0" << endl;
					break;
				}
				if ( i+2 >= max ) 						// If the index has surpassed the max number, then the input is a prime number and outputs 1.
					cout << "1" << endl;	
			}
		}
	}
	return 0;
}
